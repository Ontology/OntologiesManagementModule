﻿using OntologiesManagementModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologiesManagementModule.Controllers
{
    public class HierarchyExportViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private bool isenabled_ClassLeft;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenClassLeft", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ClassLeft
        {
            get { return isenabled_ClassLeft; }
            set
            {
                if (isenabled_ClassLeft == value) return;

                isenabled_ClassLeft = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ClassLeft);

            }
        }

        private bool istoggled_ClassLeft;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenClassLeft", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_ClassLeft
        {
            get { return istoggled_ClassLeft; }
            set
            {
                if (istoggled_ClassLeft == value) return;

                istoggled_ClassLeft = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_ClassLeft);

            }
        }

        private string datatext_ClassLeft;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpClassLeft", ViewItemType = ViewItemType.Content)]
		public string DataText_ClassLeft
        {
            get { return datatext_ClassLeft; }
            set
            {
                if (datatext_ClassLeft == value) return;

                datatext_ClassLeft = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassLeft);

            }
        }

        private bool isenabled_RelationType;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenRelationType", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_RelationType
        {
            get { return isenabled_RelationType; }
            set
            {
                if (isenabled_RelationType == value) return;

                isenabled_RelationType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_RelationType);

            }
        }

        private bool istoggled_RelationType;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenRelationType", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_RelationType
        {
            get { return istoggled_RelationType; }
            set
            {
                if (istoggled_RelationType == value) return;

                istoggled_RelationType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_RelationType);

            }
        }

        private string datatext_RelationType;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpRelationType", ViewItemType = ViewItemType.Content)]
		public string DataText_RelationType
        {
            get { return datatext_RelationType; }
            set
            {
                if (datatext_RelationType == value) return;

                datatext_RelationType = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationType);

            }
        }

        private bool isenabled_ClassRight;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenClassRight", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ClassRight
        {
            get { return isenabled_ClassRight; }
            set
            {
                if (isenabled_ClassRight == value) return;

                isenabled_ClassRight = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ClassRight);

            }
        }

        private bool istoggled_ClassRight;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenClassRight", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_ClassRight
        {
            get { return istoggled_ClassRight; }
            set
            {
                if (istoggled_ClassRight == value) return;

                istoggled_ClassRight = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_ClassRight);

            }
        }

        private string datatext_ClassRight;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpClassRight", ViewItemType = ViewItemType.Content)]
		public string DataText_ClassRight
        {
            get { return datatext_ClassRight; }
            set
            {
                if (datatext_ClassRight == value) return;

                datatext_ClassRight = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassRight);

            }
        }

        private bool isenabled_Direction;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenDirection", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Direction
        {
            get { return isenabled_Direction; }
            set
            {
                if (isenabled_Direction == value) return;

                isenabled_Direction = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Direction);

            }
        }

        private bool istoggled_Direction;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenDirection", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_Direction
        {
            get { return istoggled_Direction; }
            set
            {
                if (istoggled_Direction == value) return;

                istoggled_Direction = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Direction);

            }
        }

        private string datatext_Direction;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpDirection", ViewItemType = ViewItemType.Content)]
		public string DataText_Direction
        {
            get { return datatext_Direction; }
            set
            {
                if (datatext_Direction == value) return;

                datatext_Direction = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Direction);

            }
        }

        private bool isenabled_RootObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenFilterRootObject", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_RootObject
        {
            get { return isenabled_RootObject; }
            set
            {
                if (isenabled_RootObject == value) return;

                isenabled_RootObject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_RootObject);

            }
        }

        private bool istoggled_RootObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "isListenFilterRootObject", ViewItemType = ViewItemType.Checked)]
		public bool IsToggled_RootObject
        {
            get { return istoggled_RootObject; }
            set
            {
                if (istoggled_RootObject == value) return;

                istoggled_RootObject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_RootObject);

            }
        }

        private string datatext_RootObject;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpFilterRootObject", ViewItemType = ViewItemType.Content)]
		public string DataText_RootObject
        {
            get { return datatext_RootObject; }
            set
            {
                if (datatext_RootObject == value) return;

                datatext_RootObject = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RootObject);

            }
        }

        private bool isenabled_ParamNameId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamId", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ParamNameId
        {
            get { return isenabled_ParamNameId; }
            set
            {
                if (isenabled_ParamNameId == value) return;

                isenabled_ParamNameId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParamNameId);

            }
        }

        private string datatext_ParamNameId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamId", ViewItemType = ViewItemType.Content)]
		public string DataText_ParamNameId
        {
            get { return datatext_ParamNameId; }
            set
            {
                if (datatext_ParamNameId == value) return;

                datatext_ParamNameId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ParamNameId);

            }
        }

        private bool isenabled_ParamNameName;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamName", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ParamNameName
        {
            get { return isenabled_ParamNameName; }
            set
            {
                if (isenabled_ParamNameName == value) return;

                isenabled_ParamNameName = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParamNameName);

            }
        }

        private string datatext_ParamNameName;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamName", ViewItemType = ViewItemType.Content)]
		public string DataText_ParamNameName
        {
            get { return datatext_ParamNameName; }
            set
            {
                if (datatext_ParamNameName == value) return;

                datatext_ParamNameName = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ParamNameName);

            }
        }

        private bool isenabled_ParamNameIdClass;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamIdClass", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ParamNameIdClass
        {
            get { return isenabled_ParamNameIdClass; }
            set
            {
                if (isenabled_ParamNameIdClass == value) return;

                isenabled_ParamNameIdClass = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParamNameIdClass);

            }
        }

        private string datatext_ParamNameIdClass;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamIdClass", ViewItemType = ViewItemType.Content)]
		public string DataText_ParamNameIdClass
        {
            get { return datatext_ParamNameIdClass; }
            set
            {
                if (datatext_ParamNameIdClass == value) return;

                datatext_ParamNameIdClass = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ParamNameIdClass);

            }
        }

        private string datatext_ParamNameNameClass;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamNameClass", ViewItemType = ViewItemType.Content)]
		public string DataText_ParamNameNameClass
        {
            get { return datatext_ParamNameNameClass; }
            set
            {
                if (datatext_ParamNameNameClass == value) return;

                datatext_ParamNameNameClass = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ParamNameNameClass);

            }
        }

        private bool isenabled_ParamNameNameClass;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamNameClass", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_ParamNameNameClass
        {
            get { return isenabled_ParamNameNameClass; }
            set
            {
                if (isenabled_ParamNameNameClass == value) return;

                isenabled_ParamNameNameClass = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParamNameNameClass);

            }
        }

        private string url_ExportUrl;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "linkDiv", ViewItemType = ViewItemType.Other)]
		public string Url_ExportUrl
        {
            get { return url_ExportUrl; }
            set
            {
                if (url_ExportUrl == value) return;

                url_ExportUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_ExportUrl);

            }
        }

        private bool isenabled_Export;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "exportHierarchy", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Export
        {
            get { return isenabled_Export; }
            set
            {
                if (isenabled_Export == value) return;

                isenabled_Export = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Export);

            }
        }

        private string datatext_ParamNameSubItems;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamSubItems", ViewItemType = ViewItemType.Content)]
		public string DataText_ParamNameSubItems
        {
            get { return datatext_ParamNameSubItems; }
            set
            {
                if (datatext_ParamNameSubItems == value) return;

                datatext_ParamNameSubItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ParamNameSubItems);

            }
        }

        private bool isenabled_ParamNameSubItems;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpParamSubItems", ViewItemType = ViewItemType.Content)]
		public bool IsEnabled_ParamNameSubItems
        {
            get { return isenabled_ParamNameSubItems; }
            set
            {
                if (isenabled_ParamNameSubItems == value) return;

                isenabled_ParamNameSubItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ParamNameSubItems);

            }
        }
    }
}
