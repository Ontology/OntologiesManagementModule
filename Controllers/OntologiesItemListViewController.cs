﻿using OntologiesManagementModule.Factories;
using OntologiesManagementModule.Models;
using OntologiesManagementModule.Services;
using OntologiesManagementModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace OntologiesManagementModule.Controllers
{
    public class OntologiesItemListViewController: OntologiesItemListViewModel, IViewController
    {
        private clsLocalConfig localConfig;

        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_Ontologies serviceAgent_Elastic;

        private OntologyItemsFactory ontologyItemsFactory;

        private clsOntologyItem oItemSelected;

        private JsonFactory jsonFactory;

        private string dataJsonFile;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public OntologiesItemListViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            PropertyChanged += OntologiesItemListViewController_PropertyChanged;

            Initialize();
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgent_Elastic = new ServiceAgent_Ontologies(localConfig);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;

            ontologyItemsFactory = new OntologyItemsFactory();
            ontologyItemsFactory.PropertyChanged += OntologyItemsFactory_PropertyChanged;

            jsonFactory = new JsonFactory();
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();


            if (serviceAgent_Elastic != null)
            {
                serviceAgent_Elastic.StopRead();
                serviceAgent_Elastic = null;
            }

            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndpointType = EndpointType.Sender
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;

            ColumnConfig_Grid = GridFactory.CreateColumnList(typeof(Models.OntologyItem));

            var ontologyItemList = new List<object>();
            dataJsonFile = Guid.NewGuid().ToString() + ".json";
            var sessionFile = webSocketServiceAgent.RequestWriteStream(dataJsonFile);
            var result = jsonFactory.CreateJsonFileOfItemList(typeof(Models.OntologyItem), ontologyItemList, sessionFile);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(Models.OntologyItem), null, null, sessionFile.FileUri.AbsolutePath);

            }
            

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void OntologyItemsFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Factory_OntologyItems)
            {
                if (ontologyItemsFactory.OntologyItems != null)
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(dataJsonFile);
                    var result = jsonFactory.CreateJsonFileOfItemList(typeof(Models.OntologyItem), ontologyItemsFactory.OntologyItems.ToList<object>(), sessionFile);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        webSocketServiceAgent.SendCommand("reloadGrid");
                    }
                }
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticService_ResultOntologyItemsOfOntology)
            {
                var result = ontologyItemsFactory.CreateOntologyItemList(serviceAgent_Elastic.Ontologies,
                    serviceAgent_Elastic.OntologiesToOntologyItems,
                    serviceAgent_Elastic.OntologyItemsToOntologyRelationRules,
                    serviceAgent_Elastic.OntologyItemsToRefs);
            }
        }

        private void OntologiesItemListViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));


            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
        }
        
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "DownloadConfigItems" && ontologyItemsFactory.OntologyItems != null && ontologyItemsFactory.OntologyItems.Any())
                {

                }



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {

                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectedClassNode && IsToggled_Listen)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;
                if (oItemSelected != null && oItemSelected.GUID == oItem.GUID) return;

                if (oItem.GUID_Parent != localConfig.OItem_class_ontologies.GUID) return;

                oItemSelected = oItem;

                
            }
            else if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;
                if (oItemSelected != null && oItemSelected.GUID == oItem.GUID) return;

                oItem = serviceAgent_Elastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Object);

                if (oItem == null || oItem.GUID_Parent != localConfig.OItem_class_ontologies.GUID) return;

                oItemSelected = oItem;

                var result = serviceAgent_Elastic.GetDataOntologyItemsOfOntology(oItemSelected);

                

            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
