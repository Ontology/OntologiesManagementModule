﻿using ImportExport_Module;
using OntologiesManagementModule.Services;
using OntologiesManagementModule.Translations;
using OntologyAppDBConnector;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using OntologyClasses.BaseClasses;

namespace OntologiesManagementModule.Controllers
{
    public class HierarchyExportController : HierarchyExportViewModel, IViewController
    {
        private clsLocalConfig localConfig;

        private WebsocketServiceAgent webSocketServiceAgent;

        private ServiceAgent_HierarchyExport serviceAgentElastic;

        private ExportWorker exportWorker;

        private TranslationController translationController = new TranslationController();

        private HierarchyParam hierarchyParam = new HierarchyParam();

        private SessionFile sessionFile;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public HierarchyExportController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            PropertyChanged += HierarchyExportController_PropertyChanged;

            Initialize();
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgent_HierarchyExport(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            exportWorker = new ExportWorker();
            exportWorker.PropertyChanged += ExportWorker_PropertyChanged;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndpointType = EndpointType.Sender
            });

        }

        private void StateMachine_loginSucceded()
        {
           
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedRelationNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;

            var ontologyItemList = new List<object>();

            hierarchyParam = new HierarchyParam
            {
                Direction = localConfig.Globals.Direction_LeftRight
            };

            hierarchyParam.PropertyChanged += HierarchyParam_PropertyChanged;

            DataText_Direction = localConfig.Globals.Direction_LeftRight.Name;

            IsEnabled_Export = false;

          

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ExportWorker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ExportWorker.ResultExportHierarchyToJson))
            {
                if (exportWorker.ResultExportHierarchyToJson.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Url_ExportUrl = sessionFile.FileUri.AbsoluteUri;
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            
        }

        private void HierarchyExportController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            if (e.PropertyName == nameof(DataText_ParamNameId))
            {
                hierarchyParam.ParamName_Id = DataText_ParamNameId;
            }
            if (e.PropertyName == nameof(DataText_ParamNameIdClass))
            {
                hierarchyParam.ParamName_IdClass = DataText_ParamNameIdClass;
            }
            if (e.PropertyName == nameof(DataText_ParamNameName))
            {
                hierarchyParam.ParamName_Name = DataText_ParamNameName;
            }
            if (e.PropertyName == nameof(DataText_ParamNameNameClass))
            {
                hierarchyParam.ParamName_NameClass = DataText_ParamNameNameClass;
            }
            if (e.PropertyName == nameof(DataText_ParamNameSubItems))
            {
                hierarchyParam.ParamName_SubItems = DataText_ParamNameSubItems;
            }

            property.ViewItem.AddValue(property.Property.GetValue(this));


            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
        }

        
        private void HierarchyParam_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            IsEnabled_Export = hierarchyParam.IsValid;
        }


        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenClassLeft"))
                {
                    IsToggled_ClassLeft = !IsToggled_ClassLeft;

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenRelationType"))
                {
                    IsToggled_RelationType = !IsToggled_RelationType;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenClassRight"))
                {
                    IsToggled_ClassRight = !IsToggled_ClassRight;

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenDirection"))
                {
                    IsToggled_Direction = !IsToggled_Direction;

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenFilterRootObject"))
                {
                    IsToggled_RootObject = !IsToggled_RootObject;

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.exportHierarchy"))
                {
                    sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    if (sessionFile != null && sessionFile.StreamWriter != null)
                    {
                        var exportWorkerTask = exportWorker.ExportHierarchyOntologyToJson(localConfig.Globals, hierarchyParam, sessionFile.StreamWriter);
                    }

                    

                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {

                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectedClassNode)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;

                oItem = serviceAgentElastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Class);

                if (IsToggled_ClassLeft)
                {
                    hierarchyParam.ClassLeft = oItem;
                    DataText_ClassLeft = oItem.Name;
                    IsToggled_ClassLeft = false;
                }
                if (IsToggled_ClassRight)
                {
                    hierarchyParam.ClassRight = oItem;
                    DataText_ClassRight = oItem.Name;
                    IsToggled_ClassRight = false;
                }

            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;

                oItem = serviceAgentElastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Object);

                if (oItem.GUID != localConfig.Globals.Direction_LeftRight.GUID && oItem.GUID != localConfig.Globals.Direction_RightLeft.GUID && IsToggled_RootObject)
                {
                    hierarchyParam.FilterRootObject = oItem;
                    DataText_RootObject = oItem.Name;
                    IsToggled_RootObject = false;
                }
                else if (oItem.GUID == localConfig.Globals.Direction_LeftRight.GUID && IsToggled_Direction)
                {
                    hierarchyParam.Direction = oItem;
                    DataText_Direction = oItem.Name;
                    IsToggled_Direction = false;
                }
                else if (oItem.GUID == localConfig.Globals.Direction_RightLeft.GUID && IsToggled_Direction)
                {
                    hierarchyParam.Direction = oItem;
                    DataText_Direction = oItem.Name;
                    IsToggled_Direction = false;
                }

            }
            else if (message.ChannelId == Channels.SelectedRelationNode)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;

                oItem = serviceAgentElastic.GetOItem(oItem.GUID, localConfig.Globals.Type_RelationType);

                if (IsToggled_RelationType)
                {
                    hierarchyParam.RelationType = oItem;
                    DataText_RelationType = oItem.Name;
                    IsToggled_RelationType = false;
                }

            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
