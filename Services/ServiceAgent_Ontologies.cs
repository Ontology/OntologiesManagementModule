﻿using OntologiesManagementModule.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologiesManagementModule.Services
{
    public class ServiceAgent_Ontologies : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private Thread getOntologyItemsOfOntologyAsync;

        private object listAccessLocker = new object();


        private List<clsOntologyItem> ontologies;
        public List<clsOntologyItem> Ontologies
        {
            get
            {
                lock(listAccessLocker)
                {
                    return ontologies;
                }
                
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologies = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_Ontologies);
            }
        }

        private List<clsObjectRel> ontologiesToRefs;
        public List<clsObjectRel> OntologiesToRefs
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologiesToRefs;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologiesToRefs = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologiesToRefs);
            }
        }

        private List<clsObjectRel> ontologiesToOntologyItems;
        public List<clsObjectRel> OntologiesToOntologyItems
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologiesToOntologyItems;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologiesToOntologyItems = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologiesToOntologyItems);
            }
        }

        
        private List<clsObjectRel> ontologyJoinsToOntologyItems;
        public List<clsObjectRel> OntologyJoinsToOntologyItems
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologyJoinsToOntologyItems;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologyJoinsToOntologyItems = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologyJoinsToOntologyItems);
            }
        }

        private List<clsObjectRel> ontologiesToOntologyJoins;
        public List<clsObjectRel> OntologiesToOntologyJoins
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologiesToOntologyJoins;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologiesToOntologyJoins = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologiesToOntologyJoins);
            }
        }

        private List<clsObjectRel> ontologyItemsToOntologyRelationRules;
        public List<clsObjectRel> OntologyItemsToOntologyRelationRules
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologyItemsToOntologyRelationRules;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologyItemsToOntologyRelationRules = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologyItemsToOntologyRelationRules);
            }
        }

        private List<clsObjectRel> ontologyItemsToRefs;
        public List<clsObjectRel> OntologyItemsToRefs
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologyItemsToRefs;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologyItemsToRefs = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologyItemsToRefs);
            }
        }

        
        private List<clsObjectRel> ontologyJoinsToOntologyRelationRules;
        public List<clsObjectRel> OntologyJoinsToOntologyRelationRules
        {
            get
            {
                lock (listAccessLocker)
                {
                    return ontologyJoinsToOntologyRelationRules;
                }
            }
            set
            {
                lock (listAccessLocker)
                {
                    ontologyJoinsToOntologyRelationRules = value;
                }
                RaisePropertyChanged(NotifyChanges.ElasticService_OntologyJoinsToOntologyRelationRules);
            }
        }

        private clsOntologyItem resultOntologyItemsOfOntology;
        public clsOntologyItem ResultOntologyItemsOfOntology
        {
            get
            {
                return resultOntologyItemsOfOntology;
            }
            set
            {
                resultOntologyItemsOfOntology = value;
                RaisePropertyChanged(NotifyChanges.ElasticService_ResultOntologyItemsOfOntology);
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);
        }

        public void StopRead()
        {
            StopReadOntologyItemsOfOntology();
        }

        private void StopReadOntologyItemsOfOntology()
        {
            if (getOntologyItemsOfOntologyAsync != null)
            {
                try
                {
                    getOntologyItemsOfOntologyAsync.Abort();
                }
                catch(Exception ex) {  }
            }
        }

        public clsOntologyItem GetDataOntologyItemsOfOntology(clsOntologyItem ontology)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            StopReadOntologyItemsOfOntology();

            getOntologyItemsOfOntologyAsync = new Thread(GetDataOntologyItemsOfOntologyAsync);
            getOntologyItemsOfOntologyAsync.Start(ontology);

            return result;
        }

        private void GetDataOntologyItemsOfOntologyAsync(object item)
        {
            var ontology = (clsOntologyItem)item;

            var result = Get_Ontologies(ontology);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultOntologyItemsOfOntology = result;
                return;
            }

            result = Get_OntologyItemsOfOntologies(Ontologies);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultOntologyItemsOfOntology = result;
                return;
            }

            result = Get_RefsOfItems(OntologiesToOntologyItems, null);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultOntologyItemsOfOntology = result;
                return;
            }

            result = Get_OntologyRelationRulesOfItems(OntologiesToOntologyItems, null);


            ResultOntologyItemsOfOntology = result;
        }

        private clsOntologyItem Get_Ontologies(clsOntologyItem ontology)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologies = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = ontology != null ? ontology.GUID : null,
                    GUID_Parent = localConfig.OItem_class_ontologies.GUID
                }
            };

            var result = dbReader.GetDataObjects(searchOntologies);
            Ontologies = dbReader.Objects1;
            return result;
        }

        private clsOntologyItem Get_OntologyRefs(List<clsOntologyItem> ontologies)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologiesToRefs = ontologies.Select(ont => new clsObjectRel
            {
                ID_Object = ont.GUID,
                ID_RelationType = localConfig.OItem_relationtype_belonging_resource.GUID
            }).ToList();

            var result = dbReader.GetDataObjectRel(searchOntologiesToRefs);
            OntologiesToRefs = dbReader.ObjectRels;
            return result;
        }

        private clsOntologyItem Get_OntologyItemsOfOntologies(List<clsOntologyItem> ontologies)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologiesToOntologyItems = ontologies.Select(ont => new clsObjectRel
            {
                ID_Object = ont.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.Globals.Class_OntologyItems.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchOntologiesToOntologyItems.Any())
            {
                result = dbReader.GetDataObjectRel(searchOntologiesToOntologyItems);
                OntologiesToOntologyItems = dbReader.ObjectRels;
            }
            else
            {
                OntologiesToOntologyItems = new List<clsObjectRel>();
            }
            return result;
        }

        private clsOntologyItem Get_OntologyItemsOfOntologyJoins(List<clsOntologyItem> ontologyJoins)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologyJoinsToOntologyItems = ontologyJoins.Select(ont => new clsObjectRel
            {
                ID_Object = ont.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.Globals.Class_OntologyItems.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchOntologyJoinsToOntologyItems.Any())
            {
                result = dbReader.GetDataObjectRel(searchOntologyJoinsToOntologyItems);
                OntologyJoinsToOntologyItems = dbReader.ObjectRels;
            }
            else
            {
                OntologyJoinsToOntologyItems = new List<clsObjectRel>();
            }
            return result;
        }

        private clsOntologyItem Get_OntologiesToOntologyJoins(List<clsOntologyItem> ontologies)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologyJoins = ontologies.Select(ont => new clsObjectRel
            {
                ID_Object = ont.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.Globals.Class_OntologyJoin.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchOntologyJoins.Any())
            { 
                result = dbReader.GetDataObjectRel(searchOntologyJoins);
                OntologiesToOntologyJoins = dbReader.ObjectRels;
            }
            else
            {
                OntologiesToOntologyJoins = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_OntologyRelationRulesOfItems(List<clsObjectRel> ontologiesToOntologyItems, List<clsObjectRel> ontologyJoinsToOntologyItems)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologyrulesOfOntologyItems = new List<clsObjectRel>();

            if (ontologiesToOntologyItems != null)
            {
                searchOntologyrulesOfOntologyItems.AddRange(ontologiesToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.Globals.Class_OntologyRelationRule.GUID
                }));
            }
            

            if (ontologyJoinsToOntologyItems != null)
            {
                searchOntologyrulesOfOntologyItems.AddRange(ontologyJoinsToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.Globals.Class_OntologyRelationRule.GUID
                }));
            }

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchOntologyrulesOfOntologyItems.Any())
            {
                result = dbReader.GetDataObjectRel(searchOntologyrulesOfOntologyItems);
                OntologyItemsToOntologyRelationRules = dbReader.ObjectRels;
            }
            else
            {
                OntologyItemsToOntologyRelationRules = new List<clsObjectRel>();
            }
            
            return result;
        }

        private clsOntologyItem Get_OntologyRelationRulesOfOntologyJoins(List<clsObjectRel> ontologiesToOntologyJoins)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologyrulesOfOntologyJoins = ontologiesToOntologyJoins.Select(ont => new clsObjectRel
            {
                ID_Object = ont.ID_Other,
                ID_RelationType = localConfig.Globals.RelationType_belonging.GUID,
                ID_Parent_Other = localConfig.Globals.Class_OntologyRelationRule.GUID
            }).ToList();
            
            var result = localConfig.Globals.LState_Success.Clone();
            if (searchOntologyrulesOfOntologyJoins.Any())
            {
                result = dbReader.GetDataObjectRel(searchOntologyrulesOfOntologyJoins);
                OntologyJoinsToOntologyRelationRules = dbReader.ObjectRels;
            }
            else
            {
                OntologyJoinsToOntologyRelationRules = new List<clsObjectRel>();
            }

            return result;
        }

        private clsOntologyItem Get_RefsOfItems(List<clsObjectRel> ontologiesToOntologyItems, List<clsObjectRel> ontologyJoinsToOntologyItems)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOntologyItemsToRefs = new List<clsObjectRel>();

            if (ontologiesToOntologyItems != null)
            {
                searchOntologyItemsToRefs.AddRange(ontologiesToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingAttribute.GUID
                }));
                searchOntologyItemsToRefs.AddRange(ontologiesToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingClass.GUID
                }));
                searchOntologyItemsToRefs.AddRange(ontologiesToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingObject.GUID
                }));
                searchOntologyItemsToRefs.AddRange(ontologiesToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingRelationType.GUID
                }));
            }


            if (ontologyJoinsToOntologyItems != null)
            {
                searchOntologyItemsToRefs.AddRange(ontologyJoinsToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingAttribute.GUID
                }));
                searchOntologyItemsToRefs.AddRange(ontologyJoinsToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingClass.GUID
                }));
                searchOntologyItemsToRefs.AddRange(ontologyJoinsToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingObject.GUID
                }));
                searchOntologyItemsToRefs.AddRange(ontologyJoinsToOntologyItems.Select(ont => new clsObjectRel
                {
                    ID_Object = ont.ID_Other,
                    ID_RelationType = localConfig.Globals.RelationType_belongingRelationType.GUID
                }));
            }

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchOntologyItemsToRefs.Any())
            {
                result = dbReader.GetDataObjectRel(searchOntologyItemsToRefs);
                OntologyItemsToRefs = dbReader.ObjectRels;
            }
            else
            {
                OntologyItemsToRefs = new List<clsObjectRel>();
            }

            return result;
        }

        public ServiceAgent_Ontologies(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
