﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologiesManagementModule.Services
{
    public class ServiceAgent_HierarchyExport : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private object serviceLocker = new object();

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            return dbReader.GetOItem(id, type);
        }

        public ServiceAgent_HierarchyExport(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }

    }
}
