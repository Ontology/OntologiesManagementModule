﻿using OntologiesManagementModule.Models;
using OntologiesManagementModule.Notifications;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologiesManagementModule.Factories
{
    public class OntologyItemsFactory : NotifyPropertyChange
    {

        private clsLogStates logStates = new clsLogStates();

        private object factoryLocker = new object();

        private List<OntologyItem> ontologyItems;
        public List<OntologyItem> OntologyItems
        {
            get
            {
                lock(factoryLocker)
                {
                    return ontologyItems;
                }
                
            }
            set
            {
                lock(factoryLocker)
                {
                    ontologyItems = value;
                }
                RaisePropertyChanged(NotifyChanges.Factory_OntologyItems);
            }
        }

        private Thread createOntologyItemListAsync;

        public clsOntologyItem CreateOntologyItemList(List<clsOntologyItem> ontologies,
            List<clsObjectRel> ontologiesToOntologyItems,
            List<clsObjectRel> relationRulesOfOntologyItems,
            List<clsObjectRel> ontologyItemsToRefs)
        {
            var listParam = new OntologyItemsThreadParam
            {
                Ontologies = ontologies,
                OntologiesToOntologyItems = ontologiesToOntologyItems,
                RelationRulesOfOntologyItems = relationRulesOfOntologyItems,
                OntologyItemsToRefs = ontologyItemsToRefs
            };

            StopReadOntologyItems();

            createOntologyItemListAsync = new Thread(CreateOntologyItemListAsync);
            createOntologyItemListAsync.Start(listParam);

            return logStates.LogState_Success.Clone();
        }

        private void CreateOntologyItemListAsync(object threadParam)
        {
            var listParameter = (OntologyItemsThreadParam)threadParam;

            var ontologyItems = (from ontology in listParameter.Ontologies
                                 join ontoItem in listParameter.OntologiesToOntologyItems on ontology.GUID equals ontoItem.ID_Object
                                 join ontoItemRef in listParameter.OntologyItemsToRefs on ontoItem.ID_Other equals ontoItemRef.ID_Object
                                 join relationRule in listParameter.RelationRulesOfOntologyItems on ontoItem.ID_Other equals relationRule.ID_Object into relationRules
                                 from relationRule in relationRules.DefaultIfEmpty()
                                 select new OntologyItem
                                 {
                                     IdOntology = ontology.GUID,
                                     NameOntology = ontology.Name,
                                     IdRow = Guid.NewGuid().ToString(),
                                     IdOntologyItem = ontoItem.ID_Other,
                                     NameOntologyItem = ontoItem.Name_Other,
                                     IdRef = ontoItemRef.ID_Other,
                                     NameRef = ontoItemRef.Name_Other,
                                     IdParentRef = ontoItemRef.ID_Parent_Other,
                                     NameParentRef = ontoItemRef.Name_Parent_Other,
                                     IdOntologyRelationRule = relationRule != null ?  relationRule.ID_Other : null,
                                     NameOntologyRelationRule = relationRule != null ? relationRule.Name_Other : null,
                                     TypeRef = ontoItemRef.Ontology
                                 }).ToList();

            OntologyItems = ontologyItems;
        }

        public void StopRead()
        {
            StopReadOntologyItems();
        }

        private void StopReadOntologyItems()
        {
            if (createOntologyItemListAsync != null)
            {
                try
                {
                    createOntologyItemListAsync.Abort();
                }
                catch(Exception ex) { }
            }
        }


    }

    class OntologyItemsThreadParam
    {
        public List<clsOntologyItem> Ontologies { get; set; }
        public List<clsObjectRel> OntologiesToOntologyItems { get; set; }
        public List<clsObjectRel> RelationRulesOfOntologyItems { get; set; }
        public List<clsObjectRel> OntologyItemsToRefs { get; set; }
    }
}
