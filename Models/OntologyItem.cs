﻿using OntologiesManagementModule.Notifications;
using OntologyAppDBConnector.Base;
using OntoMsg_Module.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologiesManagementModule.Models
{
    public class OntologyItem : NotifyPropertyChange
    {
        private string idRow;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = true, IsVisible = false)]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRow);

            }
        }

        private string idOntology;
        [Json()]
        [DataViewColumn(IsIdField = false, IsVisible = false)]
        public string IdOntology
        {
            get { return idOntology; }
            set
            {
                if (idOntology == value) return;

                idOntology = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdOntology);

            }
        }

        private string nameOntology;
        [Json()]
        [DataViewColumn(Caption = "Ontology", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        public string NameOntology
        {
            get { return nameOntology; }
            set
            {
                if (nameOntology == value) return;

                nameOntology = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameOntology);

            }
        }

        private string idOntologyJoin;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdOntologyJoin
        {
            get { return idOntologyJoin; }
            set
            {
                if (idOntologyJoin == value) return;

                idOntologyJoin = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IdOntologyJoin);

            }
        }

        private string nameOntologyJoin;
        [Json()]
        [DataViewColumn(Caption = "Ontology-Join", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        public string NameOntologyJoin
        {
            get { return nameOntologyJoin; }
            set
            {
                if (nameOntologyJoin == value) return;

                nameOntologyJoin = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_NameOntologyJoin);

            }
        }

        private string idOntologyItem;
        [Json()]
        [DataViewColumn(IsIdField = false, IsVisible = false)]
        public string IdOntologyItem
        {
            get { return idOntologyItem; }
            set
            {
                if (idOntologyItem == value) return;

                idOntologyItem = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdOntologyItem);

            }
        }

        private string nameOntologyItem;
        [Json()]
        [DataViewColumn(Caption = "Item", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        public string NameOntologyItem
        {
            get { return nameOntologyItem; }
            set
            {
                if (nameOntologyItem == value) return;

                nameOntologyItem = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameOntologyItem);

            }
        }

        private string idRef;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdRef
        {
            get { return idRef; }
            set
            {
                if (idRef == value) return;

                idRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdRef);

            }
        }

        private string nameRef;
        [Json()]
        [DataViewColumn(Caption = "Reference", CellType = CellType.String, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        public string NameRef
        {
            get { return nameRef; }
            set
            {
                if (nameRef == value) return;

                nameRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameRef);

            }
        }

        private string idParentRef;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdParentRef
        {
            get { return idParentRef; }
            set
            {
                if (idParentRef == value) return;

                idParentRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdParentRef);

            }
        }

        private string nameParentRef;
        [Json()]
        [DataViewColumn(Caption = "Reference-Parent", CellType = CellType.String, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        public string NameParentRef
        {
            get { return nameParentRef; }
            set
            {
                if (nameParentRef == value) return;

                nameParentRef = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_NameParentRef);

            }
        }

        private string typeRef;
        [Json()]
        [DataViewColumn(Caption = "Reference-Type", CellType = CellType.String, DisplayOrder = 5, IsIdField = false, IsVisible = true)]
        public string TypeRef
        {
            get { return typeRef; }
            set
            {
                if (typeRef == value) return;

                typeRef = value;

                RaisePropertyChanged(NotifyChanges.DataModel_TypeRef);

            }
        }

        private string idOntologyRelationRule;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdOntologyRelationRule
        {
            get { return idOntologyRelationRule; }
            set
            {
                if (idOntologyRelationRule == value) return;

                idOntologyRelationRule = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdOntologyRelationRule);

            }
        }

        private string nameOntologyRelationRule;
        [Json()]
        [DataViewColumn(Caption = "Relation-Rule", CellType = CellType.String, DisplayOrder = 6, IsIdField = false, IsVisible = true)]
        public string NameOntologyRelationRule
        {
            get { return nameOntologyRelationRule; }
            set
            {
                if (nameOntologyRelationRule == value) return;

                nameOntologyRelationRule = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameOntologyRelationRule);

            }
        }

        private string idOItemCreationRule;
        [Json()]
        [DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdOItemCreationRule
        {
            get { return idOItemCreationRule; }
            set
            {
                if (idOItemCreationRule == value) return;

                idOItemCreationRule = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IdOItemCreationRule);

            }
        }

        private string nameOItemCreationRule;
        [Json()]
        [DataViewColumn(Caption = "Creation-Rule", CellType = CellType.String, DisplayOrder = 7, IsIdField = false, IsVisible = true)]
        public string NameOItemCreationRule
        {
            get { return nameOItemCreationRule; }
            set
            {
                if (nameOItemCreationRule == value) return;

                nameOItemCreationRule = value;

                RaisePropertyChanged(NotifyChanges.DataModel_NameOItemCreationRule);

            }
        }

        private string orderId;
        [Json()]
        [DataViewColumn(Caption = "Order-Id", CellType = CellType.Integer, DisplayOrder = 8, IsIdField = false, IsVisible = true)]
        public string OrderId
        {
            get { return orderId; }
            set
            {
                if (orderId == value) return;

                orderId = value;

                RaisePropertyChanged(NotifyChanges.DataModel_OrderId);

            }
        }
    }
}
